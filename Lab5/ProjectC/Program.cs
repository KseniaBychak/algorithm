﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProjectC
{
    internal class Program
    {
        static void Main()
        {
            Console.Title = "Сортування двохзв'язного списку вставками";
            Console.Write("Size of LinkedList:");
            uint size;
            while (uint.TryParse(Console.ReadLine(), out size) == false) Console.Write("Value isn't right!Size of LinkedList:");//вводимо розмір списку і перевіряємо тип даних
            LinkedList<int> list = new LinkedList<int>();
            Random rand = new Random();
            LinkedListNode<int> node;
            Console.WriteLine("Unsorted list:");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();// початок відліку
            for (int i = 0; i < size; i++)
            {
                list.AddLast(rand.Next(-100, 100));
                node = list.Last;
                Console.Write($"{node.Value}\t");
                for (int j = 1; j < list.Count; j++)
                {
                    LinkedListNode<int> node2 = node.Previous;
                    if (node.Value < node2.Value)
                    {
                        list.Remove(node);
                        list.AddBefore(node2, node);
                    }
                }
            }
            stopwatch.Stop();// зупинка таймера
            Console.WriteLine();
            Console.WriteLine("Sorted list:");
            node = list.First;
            for (int i = 0; i < size; i++)
            {
                Console.Write($"{node.Value}\t");
                node = node.Next;
            }
            Console.WriteLine();
            Console.WriteLine(stopwatch.Elapsed);
        }
    }
}

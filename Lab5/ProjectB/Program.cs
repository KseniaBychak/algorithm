﻿using System;
using System.Diagnostics;

namespace ProjectB
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Сортування масиву вставками";
            Console.Write("Size of array:");
            uint size;
            while (uint.TryParse(Console.ReadLine(), out size) == false) Console.Write("Value isn't right!Size of array:");//вводимо розмір масиву і перевіряємо тип даних
            int[] arr=new int[size];
            Random rand = new Random();
            int tmp;
            Stopwatch stopwatch = new Stopwatch();
            Console.WriteLine("Unsorted array:");
            for (int i = 0; i < size; i++)// заповнення масиву числами
            {
                arr[i] = rand.Next(-100, 100);
                Console.Write($"{arr[i]}\t");
            }
            stopwatch.Start();// початок відліку
            for (int i = 0; i < size; i++)
            {
                for (int j = i; j > 0; j--)// порівнюмо значення останнього доданого значення з попередніми
                {
                    if (arr[j] < arr[j - 1])
                    {
                        tmp=arr[j];
                        arr[j] = arr[j-1];
                        arr[j-1] = tmp;
                    }
                    else break;
                }
            }
            stopwatch.Stop();// зупинка таймера
            Console.WriteLine();
            Console.WriteLine("Sorted array:");
            for (int i = 0; i < size; i++) Console.Write($"{arr[i]}\t");
            Console.WriteLine();
            Console.WriteLine(stopwatch.Elapsed);// виведення часу
        }
    }
}

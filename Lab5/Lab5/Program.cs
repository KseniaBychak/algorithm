﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Lab5
{
    internal class Program
    {
        static void Main()
        {
            Console.Title = "Сортування двохзв'язного списку вибором";
            Console.Write("Size of LinkedList:");
            uint size;
            while (uint.TryParse(Console.ReadLine(), out size) == false) Console.Write("Value isn't right!Size of LinkedList:");//вводимо розмір списку і перевіряємо тип даних
            LinkedList<int> list = new LinkedList<int>();
            Random rand = new Random();
            LinkedListNode<int> node;//вузел з мінімальним значенням
            LinkedListNode<int> node2=null;
            Stopwatch stopwatch = new Stopwatch();
            Console.WriteLine("Unsorted list:");
            for (int i = 0; i < size; i++)// заповнення списку числами
            {
                list.AddLast(rand.Next(-100, 100));
                Console.Write($"{list.Last.Value}\t");
            }
            uint r = 0;// кількість посортованих елементів
            stopwatch.Start();// початок відліку
            for (uint i = 0; i < size; i++)
            {
                node = null;
                for (uint j = 0; j < size-r; j++)//знаходження мінімального числа
                {
                    if (node == null)
                    {
                        node = list.First;
                        node2 = list.First;
                    }
                    else
                    {
                        node2 = node2.Next;// проходження по вузлам
                        if (node.Value > node2.Value) node = node2;// встановлення мінімального значення
                    }
                }
                list.Remove(node);
                list.AddLast(node);//переміщення вузла в кінець
                r++;
            }
            stopwatch.Stop();// зупинка таймера
            Console.WriteLine(stopwatch.Elapsed);// вивід часу
            Console.WriteLine();
            Console.WriteLine("Sorted list:");
            node = list.First;
            for (uint i = 0; i < size; i++)
            {
                Console.Write($"{node.Value}\t");
                node = node.Next;
            }
        }
    }
}

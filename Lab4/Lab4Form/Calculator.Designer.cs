﻿namespace Lab4Form
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxString = new System.Windows.Forms.TextBox();
            this.ButtonClear = new System.Windows.Forms.Button();
            this.ButtonEqual = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.Button0 = new System.Windows.Forms.Button();
            this.ButtonDot = new System.Windows.Forms.Button();
            this.ButtonPlus = new System.Windows.Forms.Button();
            this.ButtonMinus = new System.Windows.Forms.Button();
            this.ButtonMul = new System.Windows.Forms.Button();
            this.ButtonPow = new System.Windows.Forms.Button();
            this.ButtonSqrt = new System.Windows.Forms.Button();
            this.ButtonDiv = new System.Windows.Forms.Button();
            this.ButtonDel = new System.Windows.Forms.Button();
            this.TextBoxPol = new System.Windows.Forms.TextBox();
            this.LabelString = new System.Windows.Forms.Label();
            this.LabelPol = new System.Windows.Forms.Label();
            this.LabelResult = new System.Windows.Forms.Label();
            this.TextBoxResult = new System.Windows.Forms.TextBox();
            this.ButtonBracketRight = new System.Windows.Forms.Button();
            this.ButtonBracketLeft = new System.Windows.Forms.Button();
            this.ButtonPol = new System.Windows.Forms.Button();
            this.СheckBoxPol = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // TextBoxString
            // 
            this.TextBoxString.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBoxString.Location = new System.Drawing.Point(12, 24);
            this.TextBoxString.Multiline = true;
            this.TextBoxString.Name = "TextBoxString";
            this.TextBoxString.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextBoxString.Size = new System.Drawing.Size(313, 61);
            this.TextBoxString.TabIndex = 0;
            this.TextBoxString.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextBoxString.WordWrap = false;
            // 
            // ButtonClear
            // 
            this.ButtonClear.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonClear.ForeColor = System.Drawing.Color.Red;
            this.ButtonClear.Location = new System.Drawing.Point(186, 258);
            this.ButtonClear.Name = "ButtonClear";
            this.ButtonClear.Size = new System.Drawing.Size(42, 35);
            this.ButtonClear.TabIndex = 1;
            this.ButtonClear.Text = "C";
            this.ButtonClear.UseVisualStyleBackColor = true;
            this.ButtonClear.Click += new System.EventHandler(this.ButtonClear_Click);
            // 
            // ButtonEqual
            // 
            this.ButtonEqual.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonEqual.Location = new System.Drawing.Point(12, 486);
            this.ButtonEqual.Name = "ButtonEqual";
            this.ButtonEqual.Size = new System.Drawing.Size(313, 42);
            this.ButtonEqual.TabIndex = 2;
            this.ButtonEqual.Text = "=";
            this.ButtonEqual.UseVisualStyleBackColor = true;
            this.ButtonEqual.Click += new System.EventHandler(this.ButtonEqual_Click);
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button1.Location = new System.Drawing.Point(11, 258);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(42, 35);
            this.Button1.TabIndex = 3;
            this.Button1.Text = "1";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button2
            // 
            this.Button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button2.Location = new System.Drawing.Point(59, 258);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(42, 35);
            this.Button2.TabIndex = 4;
            this.Button2.Text = "2";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button3
            // 
            this.Button3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button3.Location = new System.Drawing.Point(107, 258);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(42, 35);
            this.Button3.TabIndex = 5;
            this.Button3.Text = "3";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Button4
            // 
            this.Button4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button4.Location = new System.Drawing.Point(11, 299);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(42, 35);
            this.Button4.TabIndex = 6;
            this.Button4.Text = "4";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Button5
            // 
            this.Button5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button5.Location = new System.Drawing.Point(59, 299);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(42, 35);
            this.Button5.TabIndex = 7;
            this.Button5.Text = "5";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // Button6
            // 
            this.Button6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button6.Location = new System.Drawing.Point(107, 299);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(42, 35);
            this.Button6.TabIndex = 8;
            this.Button6.Text = "6";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // Button7
            // 
            this.Button7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button7.Location = new System.Drawing.Point(11, 340);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(42, 35);
            this.Button7.TabIndex = 9;
            this.Button7.Text = "7";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // Button8
            // 
            this.Button8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button8.Location = new System.Drawing.Point(59, 340);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(42, 35);
            this.Button8.TabIndex = 10;
            this.Button8.Text = "8";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // Button9
            // 
            this.Button9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button9.Location = new System.Drawing.Point(107, 340);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(42, 35);
            this.Button9.TabIndex = 11;
            this.Button9.Text = "9";
            this.Button9.UseVisualStyleBackColor = true;
            this.Button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // Button0
            // 
            this.Button0.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Button0.Location = new System.Drawing.Point(59, 381);
            this.Button0.Name = "Button0";
            this.Button0.Size = new System.Drawing.Size(42, 35);
            this.Button0.TabIndex = 12;
            this.Button0.Text = "0";
            this.Button0.UseVisualStyleBackColor = true;
            this.Button0.Click += new System.EventHandler(this.Button0_Click);
            // 
            // ButtonDot
            // 
            this.ButtonDot.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDot.Location = new System.Drawing.Point(107, 381);
            this.ButtonDot.Name = "ButtonDot";
            this.ButtonDot.Size = new System.Drawing.Size(42, 35);
            this.ButtonDot.TabIndex = 13;
            this.ButtonDot.Text = ".";
            this.ButtonDot.UseVisualStyleBackColor = true;
            this.ButtonDot.Click += new System.EventHandler(this.ButtonDot_Click);
            // 
            // ButtonPlus
            // 
            this.ButtonPlus.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPlus.Location = new System.Drawing.Point(186, 299);
            this.ButtonPlus.Name = "ButtonPlus";
            this.ButtonPlus.Size = new System.Drawing.Size(42, 35);
            this.ButtonPlus.TabIndex = 14;
            this.ButtonPlus.Text = "+";
            this.ButtonPlus.UseVisualStyleBackColor = true;
            this.ButtonPlus.Click += new System.EventHandler(this.ButtonPlus_Click);
            // 
            // ButtonMinus
            // 
            this.ButtonMinus.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonMinus.Location = new System.Drawing.Point(186, 340);
            this.ButtonMinus.Name = "ButtonMinus";
            this.ButtonMinus.Size = new System.Drawing.Size(42, 35);
            this.ButtonMinus.TabIndex = 15;
            this.ButtonMinus.Text = "-";
            this.ButtonMinus.UseVisualStyleBackColor = true;
            this.ButtonMinus.Click += new System.EventHandler(this.ButtonMinus_Click);
            // 
            // ButtonMul
            // 
            this.ButtonMul.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonMul.Location = new System.Drawing.Point(234, 299);
            this.ButtonMul.Name = "ButtonMul";
            this.ButtonMul.Size = new System.Drawing.Size(42, 35);
            this.ButtonMul.TabIndex = 16;
            this.ButtonMul.Text = "*";
            this.ButtonMul.UseVisualStyleBackColor = true;
            this.ButtonMul.Click += new System.EventHandler(this.ButtonMul_Click);
            // 
            // ButtonPow
            // 
            this.ButtonPow.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPow.Location = new System.Drawing.Point(282, 340);
            this.ButtonPow.Name = "ButtonPow";
            this.ButtonPow.Size = new System.Drawing.Size(42, 35);
            this.ButtonPow.TabIndex = 17;
            this.ButtonPow.Text = "^";
            this.ButtonPow.UseVisualStyleBackColor = true;
            this.ButtonPow.Click += new System.EventHandler(this.ButtonPow_Click);
            // 
            // ButtonSqrt
            // 
            this.ButtonSqrt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSqrt.Location = new System.Drawing.Point(187, 381);
            this.ButtonSqrt.Name = "ButtonSqrt";
            this.ButtonSqrt.Size = new System.Drawing.Size(137, 35);
            this.ButtonSqrt.TabIndex = 18;
            this.ButtonSqrt.Text = "sqrt";
            this.ButtonSqrt.UseVisualStyleBackColor = true;
            this.ButtonSqrt.Click += new System.EventHandler(this.ButtonSqrt_Click);
            // 
            // ButtonDiv
            // 
            this.ButtonDiv.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDiv.Location = new System.Drawing.Point(234, 340);
            this.ButtonDiv.Name = "ButtonDiv";
            this.ButtonDiv.Size = new System.Drawing.Size(42, 35);
            this.ButtonDiv.TabIndex = 19;
            this.ButtonDiv.Text = "/";
            this.ButtonDiv.UseVisualStyleBackColor = true;
            this.ButtonDiv.Click += new System.EventHandler(this.ButtonDiv_Click);
            // 
            // ButtonDel
            // 
            this.ButtonDel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDel.ForeColor = System.Drawing.Color.Red;
            this.ButtonDel.Location = new System.Drawing.Point(235, 258);
            this.ButtonDel.Name = "ButtonDel";
            this.ButtonDel.Size = new System.Drawing.Size(42, 35);
            this.ButtonDel.TabIndex = 20;
            this.ButtonDel.Text = "D";
            this.ButtonDel.UseVisualStyleBackColor = true;
            this.ButtonDel.Click += new System.EventHandler(this.ButtonDel_Click);
            // 
            // TextBoxPol
            // 
            this.TextBoxPol.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBoxPol.Location = new System.Drawing.Point(12, 111);
            this.TextBoxPol.Multiline = true;
            this.TextBoxPol.Name = "TextBoxPol";
            this.TextBoxPol.ReadOnly = true;
            this.TextBoxPol.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextBoxPol.Size = new System.Drawing.Size(313, 59);
            this.TextBoxPol.TabIndex = 21;
            this.TextBoxPol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextBoxPol.WordWrap = false;
            // 
            // LabelString
            // 
            this.LabelString.AutoSize = true;
            this.LabelString.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelString.Location = new System.Drawing.Point(92, 1);
            this.LabelString.Name = "LabelString";
            this.LabelString.Size = new System.Drawing.Size(154, 20);
            this.LabelString.TabIndex = 22;
            this.LabelString.Text = "Звичайний запис";
            // 
            // LabelPol
            // 
            this.LabelPol.AutoSize = true;
            this.LabelPol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelPol.Location = new System.Drawing.Point(55, 88);
            this.LabelPol.Name = "LabelPol";
            this.LabelPol.Size = new System.Drawing.Size(241, 20);
            this.LabelPol.TabIndex = 23;
            this.LabelPol.Text = "Зворотній польський запис";
            // 
            // LabelResult
            // 
            this.LabelResult.AutoSize = true;
            this.LabelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelResult.Location = new System.Drawing.Point(77, 173);
            this.LabelResult.Name = "LabelResult";
            this.LabelResult.Size = new System.Drawing.Size(191, 20);
            this.LabelResult.TabIndex = 25;
            this.LabelResult.Text = "Результат обчислень";
            // 
            // TextBoxResult
            // 
            this.TextBoxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBoxResult.Location = new System.Drawing.Point(11, 196);
            this.TextBoxResult.Multiline = true;
            this.TextBoxResult.Name = "TextBoxResult";
            this.TextBoxResult.ReadOnly = true;
            this.TextBoxResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TextBoxResult.Size = new System.Drawing.Size(314, 56);
            this.TextBoxResult.TabIndex = 24;
            this.TextBoxResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TextBoxResult.WordWrap = false;
            // 
            // ButtonBracketRight
            // 
            this.ButtonBracketRight.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBracketRight.Location = new System.Drawing.Point(282, 299);
            this.ButtonBracketRight.Name = "ButtonBracketRight";
            this.ButtonBracketRight.Size = new System.Drawing.Size(42, 35);
            this.ButtonBracketRight.TabIndex = 26;
            this.ButtonBracketRight.Text = ")";
            this.ButtonBracketRight.UseVisualStyleBackColor = true;
            this.ButtonBracketRight.Click += new System.EventHandler(this.ButtonBracketRight_Click);
            // 
            // ButtonBracketLeft
            // 
            this.ButtonBracketLeft.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBracketLeft.Location = new System.Drawing.Point(282, 258);
            this.ButtonBracketLeft.Name = "ButtonBracketLeft";
            this.ButtonBracketLeft.Size = new System.Drawing.Size(42, 35);
            this.ButtonBracketLeft.TabIndex = 27;
            this.ButtonBracketLeft.Text = "(";
            this.ButtonBracketLeft.UseVisualStyleBackColor = true;
            this.ButtonBracketLeft.Click += new System.EventHandler(this.ButtonBracketLeft_Click);
            // 
            // ButtonPol
            // 
            this.ButtonPol.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonPol.Location = new System.Drawing.Point(12, 422);
            this.ButtonPol.Name = "ButtonPol";
            this.ButtonPol.Size = new System.Drawing.Size(313, 58);
            this.ButtonPol.TabIndex = 28;
            this.ButtonPol.Text = "Перетворити звичайний запис в польський";
            this.ButtonPol.UseVisualStyleBackColor = true;
            this.ButtonPol.Click += new System.EventHandler(this.ButtonPol_Click);
            // 
            // СheckBoxPol
            // 
            this.СheckBoxPol.AutoSize = true;
            this.СheckBoxPol.Location = new System.Drawing.Point(12, 534);
            this.СheckBoxPol.Name = "СheckBoxPol";
            this.СheckBoxPol.Size = new System.Drawing.Size(312, 20);
            this.СheckBoxPol.TabIndex = 29;
            this.СheckBoxPol.Text = "Розблокувати поле для польського запису";
            this.СheckBoxPol.UseVisualStyleBackColor = true;
            this.СheckBoxPol.CheckedChanged += new System.EventHandler(this.СheckBoxPol_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(7, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(318, 252);
            this.panel1.TabIndex = 30;
            this.panel1.Visible = false;
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(337, 558);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.СheckBoxPol);
            this.Controls.Add(this.ButtonPol);
            this.Controls.Add(this.ButtonBracketLeft);
            this.Controls.Add(this.ButtonBracketRight);
            this.Controls.Add(this.LabelResult);
            this.Controls.Add(this.TextBoxResult);
            this.Controls.Add(this.LabelPol);
            this.Controls.Add(this.LabelString);
            this.Controls.Add(this.TextBoxPol);
            this.Controls.Add(this.ButtonDel);
            this.Controls.Add(this.ButtonDiv);
            this.Controls.Add(this.ButtonSqrt);
            this.Controls.Add(this.ButtonPow);
            this.Controls.Add(this.ButtonMul);
            this.Controls.Add(this.ButtonMinus);
            this.Controls.Add(this.ButtonPlus);
            this.Controls.Add(this.ButtonDot);
            this.Controls.Add(this.Button0);
            this.Controls.Add(this.Button9);
            this.Controls.Add(this.Button8);
            this.Controls.Add(this.Button7);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.ButtonEqual);
            this.Controls.Add(this.ButtonClear);
            this.Controls.Add(this.TextBoxString);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.Load += new System.EventHandler(this.Calculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxString;
        private System.Windows.Forms.Button ButtonClear;
        private System.Windows.Forms.Button ButtonEqual;
        private System.Windows.Forms.Button Button1;
        private System.Windows.Forms.Button Button2;
        private System.Windows.Forms.Button Button3;
        private System.Windows.Forms.Button Button4;
        private System.Windows.Forms.Button Button5;
        private System.Windows.Forms.Button Button6;
        private System.Windows.Forms.Button Button7;
        private System.Windows.Forms.Button Button8;
        private System.Windows.Forms.Button Button9;
        private System.Windows.Forms.Button Button0;
        private System.Windows.Forms.Button ButtonDot;
        private System.Windows.Forms.Button ButtonPlus;
        private System.Windows.Forms.Button ButtonMinus;
        private System.Windows.Forms.Button ButtonMul;
        private System.Windows.Forms.Button ButtonPow;
        private System.Windows.Forms.Button ButtonSqrt;
        private System.Windows.Forms.Button ButtonDiv;
        private System.Windows.Forms.Button ButtonDel;
        private System.Windows.Forms.TextBox TextBoxPol;
        private System.Windows.Forms.Label LabelString;
        private System.Windows.Forms.Label LabelPol;
        private System.Windows.Forms.Label LabelResult;
        private System.Windows.Forms.TextBox TextBoxResult;
        private System.Windows.Forms.Button ButtonBracketRight;
        private System.Windows.Forms.Button ButtonBracketLeft;
        private System.Windows.Forms.Button ButtonPol;
        private System.Windows.Forms.CheckBox СheckBoxPol;
        private System.Windows.Forms.Panel panel1;
    }
}


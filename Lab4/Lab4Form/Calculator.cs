﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Lab4Form
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Calculator_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < flarr.Length; i++)
            {
                flarr[i] = false;
            }
        }
        bool[] flarr = new bool[100];
        int flcount = 0;
        int flb = 0;
        LinkedList<string> stack = new LinkedList<string>();
        LinkedList<string> queue = new LinkedList<string>();
        string o = "+^-/*";

        private void Button1_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "1";
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "2";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "3";
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "4";
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "5";
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "6";
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "7";
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "8";
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "9";
        }

        private void Button0_Click(object sender, EventArgs e)
        {
            TextBoxString.Text += "0";
        }

        private void ButtonDot_Click(object sender, EventArgs e)
        {
            if (TextBoxString.Text != "" && TextBoxString.Text.EndsWith(" ")==false)
            {
                if (flarr[flcount] == false)
                {
                    TextBoxString.Text += ".";
                    flarr[flcount] = true;
                }
            }
        }

        private void ButtonDel_Click(object sender, EventArgs e)
        {
            if(TextBoxString.Text.EndsWith("."))
            {
                flarr[flcount] = false;
                TextBoxString.Text=TextBoxString.Text.Remove(TextBoxString.Text.Length - 1);
            }
            else if (TextBoxString.Text.EndsWith(" "))
            {
                flcount--;
                TextBoxString.Text = TextBoxString.Text.Remove(TextBoxString.Text.Length - 3);
            }else if(TextBoxString.Text.EndsWith("sqrt"))
            {
                TextBoxString.Text = TextBoxString.Text.Remove(TextBoxString.Text.Length - 4);
            }
            else if(TextBoxString.Text != "")
            {
                TextBoxString.Text = TextBoxString.Text.Remove(TextBoxString.Text.Length - 1);
            }
        }

        private void ButtonClear_Click(object sender, EventArgs e)
        {
            TextBoxString.Text = "";
            for (int i = flcount; i >=0; i--)
            {
                flarr[i] = false;
            }
            flcount = 0;
            TextBoxResult.Text = "";
            TextBoxPol.Text = "";
        }

        private void ButtonPlus_Click(object sender, EventArgs e)
        {
            if (Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1]) || TextBoxString.Text[TextBoxString.Text.Length - 2] == ')')
            {
                TextBoxString.Text += " + ";
                flcount++;
            }
        }

        private void ButtonDiv_Click(object sender, EventArgs e)
        {
            if (Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1]) || TextBoxString.Text[TextBoxString.Text.Length - 2]==')')
            {
                TextBoxString.Text += " / ";
                flcount++;
            }
        }

        private void ButtonMinus_Click(object sender, EventArgs e)
        {
            if (Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1]) || TextBoxString.Text[TextBoxString.Text.Length - 2] == ')')
            {
                TextBoxString.Text += " - ";
                flcount++;
            }else if(o.Contains($"{TextBoxString.Text[TextBoxString.Text.Length - 2]}")) TextBoxString.Text += "-";
        }

        private void ButtonPow_Click(object sender, EventArgs e)
        {
            if (Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1]) || TextBoxString.Text[TextBoxString.Text.Length - 2] == ')')
            {
                TextBoxString.Text += " ^ ";
                flcount++;
            }
        }

        private void ButtonMul_Click(object sender, EventArgs e)
        {
            if (Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1]) || TextBoxString.Text[TextBoxString.Text.Length - 2] == ')')
            {
                TextBoxString.Text += " * ";
                flcount++;
            }
        }

        private void ButtonSqrt_Click(object sender, EventArgs e)
        {
            if(TextBoxString.Text == "") TextBoxString.Text += " sqrt ";
            else if (TextBoxString.Text[TextBoxString.Text.Length - 1]==' ')
            {
                TextBoxString.Text += "sqrt ";
                flcount++;
            }
        }

        private void ButtonBracketLeft_Click(object sender, EventArgs e)
        {
            if (TextBoxString.Text=="" || Char.IsDigit(TextBoxString.Text[TextBoxString.Text.Length - 1])==false|| TextBoxString.Text[TextBoxString.Text.Length - 2] == ')')
            {
                if (TextBoxString.Text[TextBoxString.Text.Length - 2] == ')') TextBoxString.Text += " * ";
                TextBoxString.Text += " ( ";
                flb++;
            }
        }

        private void ButtonBracketRight_Click(object sender, EventArgs e)
        {
            if (o.Contains($"{TextBoxString.Text[TextBoxString.Text.Length - 2]}")==false && flb>0)
            {
                TextBoxString.Text += " ) ";
                flb--;
            }
        }

        private void ButtonPol_Click(object sender, EventArgs e)
        {
            if ((TextBoxString.Text.EndsWith(" ") && TextBoxString.Text.EndsWith(") ") == false) || TextBoxString.Text=="") TextBoxPol.Text = "Помилка!Введіть число!";
            else if (flb !=0) TextBoxPol.Text = "Помилка!Закрийте дужку!";
            else
            {
                TextBoxPol.Text = "";
                string[] strs = TextBoxString.Text.Split(' ');
                string o1 = "+-";
                string o2 = "*/";
                for (int i = 0; i < strs.Length; i++)
                {
                    if (strs[i] == "") continue;
                    if (strs[i] == "sqrt" || strs[i] == "(") stack.AddLast(strs[i]);
                    else if (strs[i] == ")")
                    {
                        while (stack.Last.Value != "(")
                        {
                            queue.AddLast(stack.Last.Value);
                            TextBoxPol.Text += queue.Last.Value + " ";
                            stack.RemoveLast();
                        }
                        stack.RemoveLast();
                    }
                    else if (o.Contains(strs[i]))
                    {
                        while (stack.Count != 0 && ((stack.Last.Value == "sqrt")|| (o1.Contains(strs[i]) && (stack.Last.Value == "^" || o2.Contains(stack.Last.Value) || stack.Last.Value == "-"))|| (o2.Contains(strs[i]) && (stack.Last.Value == "^" || stack.Last.Value == "/"))))
                        {
                                queue.AddLast(stack.Last.Value);
                                TextBoxPol.Text += queue.Last.Value + " ";
                                stack.RemoveLast();
                        }
                        stack.AddLast(strs[i]);
                    }
                    else
                    {
                        queue.AddLast(strs[i]);
                        TextBoxPol.Text += queue.Last.Value + " ";
                    }
                }
                while(stack.Count!=0)
                {
                    queue.AddLast(stack.Last.Value);
                    TextBoxPol.Text += stack.Last.Value + " ";
                    stack.RemoveLast();
                }
                stack.Clear();
                queue.Clear();
            }
        }

        private void ButtonEqual_Click(object sender, EventArgs e)
        {
            TextBoxResult.Text = "";
            if (TextBoxPol.Text == "") TextBoxResult.Text = "Спочатку введіть чи отримайте польський запис!";
            else
            {
                int dig = 0, sign=0;
                string [] strs= TextBoxPol.Text.Split(' ');
                for(int i = 0; i < strs.Length; i++)
                {
                    if (strs[i] == "") continue;
                    if (o.Contains(strs[i])) sign++;
                    else if(strs[i]!="sqrt") dig++;
                }
                if (dig-sign-1 != 0) TextBoxResult.Text = "Вираз введений неправильно!";
                else
                {
                    for (int i = 0; i < strs.Length; i++)
                    {
                        if (strs[i] != "") queue.AddLast(strs[i]);
                    }
                    int q = queue.Count;
                    string first, second;
                    while (queue.Count > 0)
                    {
                        if (o.Contains(queue.First.Value))
                        {
                            first = stack.Last.Value;
                            stack.RemoveLast();
                            second = stack.Last.Value;
                            stack.RemoveLast();
                            if (queue.First.Value == "+") stack.AddLast((double.Parse(second) + double.Parse(first)).ToString());
                            else if (queue.First.Value == "-") stack.AddLast((double.Parse(second) - double.Parse(first)).ToString());
                            else if (queue.First.Value == "*") stack.AddLast((double.Parse(second) * double.Parse(first)).ToString());
                            else if (queue.First.Value == "/")
                            {
                                if (double.Parse(first) == 0)
                                {
                                    stack.AddLast("На нуль ділити не можна!");
                                    break;
                                }
                                else stack.AddLast((double.Parse(second) / double.Parse(first)).ToString());
                            }
                            else stack.AddLast(Math.Pow(double.Parse(second), double.Parse(first)).ToString());
                        }
                        else if (queue.First.Value == "sqrt")
                        {
                            first = stack.Last.Value;
                            if (double.Parse(first) < 0)
                            {
                                stack.Last.Value = "Значення виразу під коренем не повинно бути менше 0!";
                                break;
                            }
                            else
                            {
                                stack.RemoveLast();
                                stack.AddLast(Math.Sqrt(double.Parse(first)).ToString());
                            }
                        }
                        else
                        {
                            stack.AddLast(queue.First.Value);
                        }
                        queue.RemoveFirst();
                    }
                    TextBoxResult.Text = stack.Last.Value;
                }
            }
            stack.Clear();
            queue.Clear();
        }

        private void СheckBoxPol_CheckedChanged(object sender, EventArgs e)
        {
            if (СheckBoxPol.Checked == true) TextBoxPol.ReadOnly = false;
            else TextBoxPol.ReadOnly = true;
        }
    }
}

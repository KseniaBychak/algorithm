﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = System.Text.Encoding.Unicode;

            double a, c, m, mat = 0, dis = 0, x;
            int k = 10000;
            int[] arr = new int[10000];//числа
            int[] count = new int[100];//частота
            double[] stat = new double[100];//статистична ймовірність
            m = Math.Pow(2, 31) / (Math.Pow(2, 31) / 100);
            c = 12345 / (Math.Pow(2, 31) / 100);
            a = 1103515245 / (Math.Pow(2, 31) / 100); Console.Write($"a=1103515245/((2^31)/100)={a:F3}\nc=12345/((2^31)/100)={c:F3}\nm=2^31/((2^31)/100)={m:F3}\nX0=");
            x = double.Parse(Console.ReadLine());
            for (int i = 0; i < k; i++)
            {
                x = (a * x + c) % m;
                arr[i] = (int)x;
                Console.Write("\nX{0}={1:F0}", i + 1, x);
            }
            Console.WriteLine("\n");
            Console.WriteLine("Частота інтервалів");
            for (int j = 0; j < 100; j++)
            {
                count[j] = 0;
                for (int i = 0; i < k; i++)
                {
                    if (arr[i] == j) count[j]++;
                }
                Console.WriteLine($"{j} зустрічається {count[j]} раз");
            }
            Console.WriteLine();
            Console.WriteLine("Статиснична ймовірність");
            for (int j = 0; j < 100; j++)
            {
                stat[j] = (double)count[j] / k;
                Console.WriteLine($"{j} - {stat[j]:F5}({stat[j] * 100:F3}*/o) ");
            }
            Console.WriteLine();
            Console.Write("Математичне сподівання");
            for (int j = 0; j < 100; j++)
            {
                mat += j * stat[j];
            }
            Console.WriteLine("={0:F3}", mat);
            Console.WriteLine();
            Console.Write("Дисперсія");
            for (int j = 0; j < 100; j++)
            {
                dis += (float)(Math.Pow(j - mat, 2) * stat[j]);
            }
            Console.WriteLine("={0:F3}", dis);
            Console.WriteLine();
            Console.Write("Середньоквадратичне відхилення");
            Console.WriteLine("={0:F3}", Math.Sqrt(dis));
        }
    }
}

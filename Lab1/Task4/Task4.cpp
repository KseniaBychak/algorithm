﻿#include <stdio.h>
#include <stdlib.h>
union floatbit {
	float number;
	struct {
		unsigned char bit0 : 1;
		unsigned char bit1 : 1;
		unsigned char bit2 : 1;
		unsigned char bit3 : 1;
		unsigned char bit4 : 1;
		unsigned char bit5 : 1;
		unsigned char bit6 : 1;
		unsigned char bit7 : 1;
		unsigned char bit8 : 1;
		unsigned char bit9 : 1;
		unsigned char bit10 : 1;
		unsigned char bit11 : 1;
		unsigned char bit12 : 1;
		unsigned char bit13 : 1;
		unsigned char bit14 : 1;
		unsigned char bit15 : 1;
		unsigned char bit16 : 1;
		unsigned char bit17 : 1;
		unsigned char bit18 : 1;
		unsigned char bit19 : 1;
		unsigned char bit20 : 1;
		unsigned char bit21 : 1;
		unsigned char bit22 : 1;
		unsigned char bit23 : 1;
		unsigned char bit24 : 1;
		unsigned char bit25 : 1;
		unsigned char bit26 : 1;
		unsigned char bit27 : 1;
		unsigned char bit28 : 1;
		unsigned char bit29 : 1;
		unsigned char bit30 : 1;
		unsigned char bit31 : 1;
	}bits;
};
union floatbyte {
	float number;
	struct {
		unsigned char byte0;
		unsigned char byte1;
		unsigned char byte2;
		unsigned char byte3;
	}bytes;
};
union floatpart {
	float number;
	struct {
		unsigned int mant : 23;
		unsigned int exp : 8;
		unsigned int sign : 1;
	}parts;
};
int main()
{
	system("chcp 1251");
	system("cls");
	float x;
	floatbit flbit;
	floatbyte flbyte;
	floatpart flpart;
	printf("Введіть число:");
	scanf_s("%f", &x);
	flbit.number = x;
	flbyte.number = x;
	flpart.number = x;
	printf("1)виведення числа побітово:");
	printf("%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d", flbit.bits.bit31, flbit.bits.bit30, flbit.bits.bit29, flbit.bits.bit28, flbit.bits.bit27, flbit.bits.bit26, flbit.bits.bit25, flbit.bits.bit24, flbit.bits.bit23, flbit.bits.bit22, flbit.bits.bit21, flbit.bits.bit20, flbit.bits.bit19, flbit.bits.bit18, flbit.bits.bit17, flbit.bits.bit16, flbit.bits.bit15, flbit.bits.bit14, flbit.bits.bit13, flbit.bits.bit12, flbit.bits.bit11, flbit.bits.bit10, flbit.bits.bit9, flbit.bits.bit8, flbit.bits.bit7, flbit.bits.bit6, flbit.bits.bit5, flbit.bits.bit4, flbit.bits.bit3, flbit.bits.bit2, flbit.bits.bit1, flbit.bits.bit0);
	printf("\nРозмір обєднання: %d бaйта", sizeof(flbit));
	printf("\n2)виведення числа побайтово:");
	printf("%x %x %x %x", flbyte.bytes.byte3, flbyte.bytes.byte2, flbyte.bytes.byte1, flbyte.bytes.byte0);
	printf("\nРозмір обєднання: %d байта", sizeof(flbyte));
	printf("\n3)виведення знака, мантиси і ступеня значення:");
	printf("%x  %x  %x", flpart.parts.sign, flpart.parts.exp, flpart.parts.mant);
	printf("\nРозмір обєднання: %d байта", sizeof(flpart));
}

﻿#include <stdio.h>
#include <time.h>
struct totaltime {
	char year[5] = "2022";
	char mounth[3] = "02";
	char day[3] = "11";
	char hours[3] = "09";
	char minutes[3] = "50";
	char seconds[3] = "10";
};
struct righttime
{
	unsigned short year : 11;
	unsigned short mounth : 4;
	unsigned short day : 5;
	unsigned short hours : 5;
	unsigned short minutes : 6;
	unsigned short seconds : 6;
};
int main()
{
	totaltime mytime;
	printf("%s.%s.%s. %s:%s:%s\n", mytime.day, mytime.mounth, mytime.year, mytime.hours, mytime.minutes, mytime.seconds);
	printf("Size of my struct (totaltime):%d bytes\n", sizeof(mytime));
	righttime mytime2;
	mytime2.year = 2022;
	mytime2.mounth = 2;
	mytime2.day = 11;
	mytime2.hours = 9;
	mytime2.minutes = 50;
	mytime2.seconds = 10;
	printf("%d.%d.%d. %d:%d:%d\n", mytime2.day, mytime2.mounth, mytime2.year, mytime2.hours, mytime2.minutes, mytime2.seconds);
	printf("Size of my struct (rightime):%d bytes\n", sizeof(mytime2));
	tm time;
	time.tm_year = 2022;
	time.tm_mon = 2;
	time.tm_mday = 11;
	time.tm_hour = 9;
	time.tm_min = 50;
	time.tm_sec = 10;
	printf("%d.%d.%d. %d:%d:%d\n", time.tm_mday, time.tm_mon, time.tm_year, time.tm_hour, time.tm_min, time.tm_sec);
	printf("Size of system struct (tm):%d bytes\n", sizeof(time));
}

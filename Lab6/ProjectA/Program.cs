﻿using System;
using System.Diagnostics;

namespace ProjectA
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Пірамідальне сортуванння";
            Console.Write("Size of array:");
            int size;
            while (int.TryParse(Console.ReadLine(), out size) == false && size < 0) Console.Write("Value isn't right!Size of array:");
            float[] heap = new float[size];
            float[] arr = new float[size];
            Random rand = new Random();
            float tmp;
            int parentindex;
            Stopwatch stopwatch = new Stopwatch();
            Console.WriteLine("Unsorted array:");
            for (int i = 0; i < size; i++)
            {
                heap[i] = (float)Math.Round((rand.Next(10, 100) + (float)rand.NextDouble()), 2);
                Console.Write($"{heap[i]}\t");
            }
            int readyel = 1;
            stopwatch.Start();
            for (int i = size - 1; i >= 0; i--)
            {
                for (int j = size - readyel; j > 0; j = j - 2)
                {
                    parentindex = (j - 1) / 2;
                    if (2 * parentindex + 2 < size - readyel + 1)
                        if (heap[2 * parentindex + 2] > heap[2 * parentindex + 1])
                        {
                            tmp = heap[2 * parentindex + 2];
                            heap[2 * parentindex + 2] = heap[2 * parentindex + 1];
                            heap[2 * parentindex + 1] = tmp;
                        }
                    if (heap[2 * parentindex + 1] > heap[parentindex])
                    {
                        tmp = heap[2 * parentindex + 1];
                        heap[2 * parentindex + 1] = heap[parentindex];
                        heap[parentindex] = tmp;
                    }
                }
                arr[i] = heap[0];
                heap[0] = heap[size - readyel];
                readyel++;
            }
            stopwatch.Stop();
            Console.WriteLine();
            Console.WriteLine("Sorted array:");
            for (int i = 0; i < size; i++) Console.Write($"{arr[i]}\t");
            Console.WriteLine();
            Console.WriteLine(stopwatch.Elapsed);
        }
    }
}

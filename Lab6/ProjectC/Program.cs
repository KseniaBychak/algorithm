﻿using System;
using System.Diagnostics;

namespace ProjectC
{
    internal class Program
    {
        static void Main()
        {
            Console.Title = "Сортування підрахунком";
            Console.Write("Size of array:");
            int size;
            while (int.TryParse(Console.ReadLine(), out size) == false && size < 0) Console.Write("Value isn't right!Size of array:");
            short[] arr = new short[size];
            short[] count = new short[111];
            Random rand = new Random();
            float tmp;
            Stopwatch stopwatch = new Stopwatch();
            Console.WriteLine("Unsorted array:");
            for (int i = 0; i < size; i++)
            {
                arr[i] = (short)rand.Next(-100, 11);
                Console.Write($"{arr[i]}\t");
            }
            stopwatch.Start();
            for(int i = 0; i < size; i++)
            {
                count[arr[i] + 100]++;
            }
            int k = 0;
            for (int i = 0; i < 111; i++)
            {
                for (int j = 0; j < count[i]; j++,k++)
                {
                    arr[k] = (short)(i - 100);
                }
            }
            stopwatch.Stop();
            Console.WriteLine();
            Console.WriteLine("Sorted array:");
            for (int i = 0; i < size; i++) Console.Write($"{arr[i]}\t");
            Console.WriteLine();
            Console.WriteLine(stopwatch.Elapsed);
        }
    }
}

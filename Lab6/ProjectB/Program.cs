﻿using System;
using System.Diagnostics;

namespace ProjectB
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Сортування Шелла";
            Console.Write("Size of array:");
            int size;
            while (int.TryParse(Console.ReadLine(), out size) == false && size < 0) Console.Write("Value isn't right!Size of array:");
            double[] arr = new double[size];
            Random rand = new Random();
            Stopwatch stopwatch = new Stopwatch();
            Console.WriteLine("Unsorted array:");
            for (int i = 0; i < size; i++)
            {
                arr[i] = Math.Round(rand.Next(10, 100) + rand.NextDouble(), 4);
                Console.Write($"{arr[i]}\t");
            }
            double temp;
            int gap=0,maxgap=0,gi=0;
            for (int i = 0; gap < size; i++)
            {
                maxgap = gap;
                gi = i;
                gap = (int)Math.Pow(2, i) + 1;
            }
            gap = maxgap;
            stopwatch.Start();
            while(gi>=-1)
            {
                for (int i = 0; i < (arr.Length - gap); i++)
                {
                    int j = i;
                    while (j >= 0 && arr[j] > arr[j + gap])
                    {
                        temp = arr[j];
                        arr[j] = arr[j + gap];
                        arr[j + gap] = temp;
                        j--;
                    }
                }
                gi--;
                gap = (int)Math.Pow(2, gi) + 1;

            }
            stopwatch.Stop();
            Console.WriteLine();
            Console.WriteLine("Sorted array:");
            for (int i = 0; i < size; i++) Console.Write($"{arr[i]}\t");
            Console.WriteLine();
            Console.WriteLine(stopwatch.Elapsed);
        }
    }
}

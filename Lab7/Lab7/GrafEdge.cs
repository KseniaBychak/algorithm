﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab7
{
    internal class GrafEdge
    {
        public GrafEdge[] LinkedEdges { get; set; }
        public string Value { get; set; }
        public int Weight { get; set; }
        public bool Used { get; set; }
        public byte Index { get; set; }
        public GrafEdge(GrafEdge[] edges,string value, byte index)
        {
            LinkedEdges = edges;
            Value = value;
            Index = index;
            Used = false;
        }
        public GrafEdge(GrafEdge[] edges, string value,int weight, byte index)
        {
            LinkedEdges = edges;
            Value = value;
            Weight = weight;
            Index = index;
            Used = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Lab7
{
    internal class Program
    {
        static Graf graf;
        static Stack<int> stack = new Stack<int>();
        static Queue<int> queue = new Queue<int>();
        static int find = -1;
        static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                     System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;
            GrafEdge [] edges= new GrafEdge[19];
            edges[18] = new GrafEdge(null,"Тернопіль", 104,18);
            edges[17] = new GrafEdge(null, "Луцьк", 68,17);
            edges[16] = new GrafEdge(new GrafEdge[1]{edges[18]}, "Хмельницький", 110,16);
            edges[15] = new GrafEdge(new GrafEdge[1] { edges[17] }, "Рівно", 100,15);
            edges[14] = new GrafEdge(new GrafEdge[1] { edges[16] }, "Вінниця", 73,14);
            edges[13] = new GrafEdge(null,"Кременчук", 105,13);
            edges[12] = new GrafEdge(null,"Харків", 130,12);
            edges[11] = new GrafEdge(new GrafEdge[1] { edges[15] }, "Новоград-Волинський",80,11);
            edges[10] = new GrafEdge(new GrafEdge[1] { edges[14] }, "Бердичів", 38,10);
            edges[9] = new GrafEdge(null,"Шепетівка", 115,9);
            edges[8] = new GrafEdge(null,"Умань", 115,8);
            edges[7] = new GrafEdge(new GrafEdge[1] { edges[13] }, "Черкаси", 146,7);
            edges[6] = new GrafEdge(new GrafEdge[1] { edges[12] }, "Полтава", 181,6);
            edges[5] = new GrafEdge(null,"Суми", 175,5);
            edges[4] = new GrafEdge(null,"Миргород", 109,4);
            edges[3] = new GrafEdge(new GrafEdge[3] {edges[9],edges[10],edges[11]},"Житомир", 135,3);
            edges[2] = new GrafEdge(new GrafEdge[3] { edges[6], edges[7], edges[8] }, "Біла Церква", 78,2);
            edges[1] = new GrafEdge(new GrafEdge[2] { edges[4], edges[5],}, "Прилуки", 128,1);
            edges[0] = new GrafEdge(new GrafEdge[3] { edges[1], edges[2], edges[3] }, "Київ",0);
            graf = new Graf(edges);
            int choose;
            do
            {
                Console.WriteLine("Виберіть дію:\n1-знайти запис за методом DFS\n2-знайти запис за методом BFS\n3-вивести можливі маршрути та їх дистанцію\n4-завершити роботу");
                while (!int.TryParse(Console.ReadLine(), out choose) || choose < 1 || choose > 4)
                {
                    Console.WriteLine("Виберіть пункт меню");
                }
                switch (choose)
                {
                    case 1:
                        Console.WriteLine("Введіть пункт призначення");
                        string place=Console.ReadLine();
                        DFS(place);
                        if (find == -1) Console.WriteLine("Даний пункт знайти не вдалося!");
                        else Console.WriteLine($"{place} є у списку під індексом {find}");
                        ClearUsed();
                        find = -1;
                        break;
                    case 2:
                        Console.WriteLine("Введіть пункт призначення");
                        place = Console.ReadLine();
                        BFS(place);
                        if (find == -1) Console.WriteLine("Даний пункт знайти не вдалося!");
                        else Console.WriteLine($"{place} є у списку під індексом {find}");
                        find = -1;
                        break;
                    case 3:
                        GetWay();
                        ClearUsed();
                        break;
                }
            } while (choose != 4);             
        }
        static void DFS(string value,int findindex=0)
        {  
            stack.Push(findindex);
            graf.Edges[findindex].Used = true; 
            if (value == graf.Edges[stack.Peek()].Value) find = findindex;
            if (find == -1 && graf.Edges[findindex].LinkedEdges!=null)
            {
                for (int i = 0; i < graf.Edges[findindex].LinkedEdges.Length; i++)
                {
                    if (!graf.Edges[findindex].LinkedEdges[i].Used) DFS(value, graf.Edges[findindex].LinkedEdges[i].Index);
                }
            }
            stack.Pop();
        }
        static void BFS(string value, int findindex=0)
        {
            if(findindex==0)queue.Enqueue(findindex);
            if (value == graf.Edges[queue.Peek()].Value)
            {
                find = findindex;
                return;
            }
            if (find == -1 && graf.Edges[findindex].LinkedEdges != null)
            {
                for (int i = 0; i < graf.Edges[findindex].LinkedEdges.Length; i++)
                {
                    if (!graf.Edges[findindex].LinkedEdges[i].Used) queue.Enqueue(graf.Edges[findindex].LinkedEdges[i].Index);
                    if (value == graf.Edges[queue.Peek()].Value)
                    {
                        find = findindex;
                        return;
                    }
                }
            }
            queue.Dequeue();
            if(queue.Count!=0)BFS(value,queue.Peek());
        }
        static void GetWay(int findindex=0,int distance=0)
        {
            bool fl=false;
            stack.Push(findindex);
            if (graf.Edges[findindex].LinkedEdges != null)
            {
                for (int i = 0; i < graf.Edges[findindex].LinkedEdges.Length; i++)
                {
                    if (!graf.Edges[findindex].LinkedEdges[i].Used)
                    {
                        GetWay(graf.Edges[findindex].LinkedEdges[i].Index);
                    }
                }
            }
            //else fl=true;
            string way = "";
            //if (fl)
            //{
                Stack<int> stack2 = new Stack<int>();
                for (int i = 0; stack.Count != 0; i++)
                {
                    stack2.Push(stack.Pop());
                }
                for (int i = 0; stack2.Count != 0; i++)
                {
                    way += $"({graf.Edges[stack2.Peek()].Weight}){graf.Edges[stack2.Peek()].Value}-";
                    distance += graf.Edges[stack2.Peek()].Weight;
                    stack.Push(stack2.Pop());
                }
                Console.WriteLine(way + $"-->{distance}км");
            //}
            graf.Edges[findindex].Used = true;
            distance -= graf.Edges[stack.Pop()].Weight;
        }
        static void ClearUsed(int index = 0)
        {
            if (index < graf.Edges.Length)
            {
                graf.Edges[index].Used = false;
                ClearUsed(index + 1);
            }
        }
    }
}

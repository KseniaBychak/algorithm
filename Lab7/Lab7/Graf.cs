﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab7
{
    internal class Graf
    {
        public GrafEdge[] Edges { get; set; }
        public Graf(GrafEdge[] edges)
        {
            Edges = edges;
        }
    }
}

﻿using System.Diagnostics;
namespace Lab3
{
    class Program
    {
        static void Main()
        {
            byte m,max=0;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.Write("Введіть розмір масиву(менше 20): m=");
            while (byte.TryParse(Console.ReadLine(), out m ) == false || m>=20 || m<=0)
            {
                Console.Write("Неправильне значення m!\nm=");
            }
            byte[] arr = new byte[m];
            Random rand = new Random();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < m; i++)
            {
                arr[i] = (byte)rand.Next(0, 10);
                if (max < arr[i]) max = arr[i];
            }
            sw.Stop();
            for (int i = 0; i < m; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine($"Максимальне число:{max}");
            Console.WriteLine($"Час виконання функції:{sw.Elapsed}");
        }
    }
}

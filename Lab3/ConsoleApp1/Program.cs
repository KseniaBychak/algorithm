﻿using System;

namespace Lab3
{
    class Program
    {
        static void Main()
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            long fact = 1;
            Console.WriteLine("f(n)=n  f(n)=log(n)  f(n)=n*log(n)   f(n)=n^2    f(n)=2^n           f(n)=n!");
            for(int i = 1; i <=50;i++)
            {
                Console.WriteLine($"{i,-8}{Math.Log10(i),-13:F3}{i* Math.Log10(i),-16:F3}{i*i,-12}{Math.Pow(2,i),-19}{Factorial(i,ref fact)}");
            }
        }
        static long Factorial(int i,ref long fact)
        {
            fact = fact * i;
            return fact;
        }
    }
}

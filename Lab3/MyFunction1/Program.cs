﻿using System.Diagnostics;
namespace Lab3
{
    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            byte a, b;
            long res;
            Console.Write("0<=a<=10\n0<=b<20\na=");
            while (byte.TryParse(Console.ReadLine(), out a) == false || a > 10)
            {
                Console.Write("Неправильне значення а!\na=");
            }
            Console.Write("b=");
            while (byte.TryParse(Console.ReadLine(), out b) == false || b >= 20)
            {
                Console.Write("Неправильне значення b!\nb=");
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (b == 0) res = 1;
            else if (b == 1) res = a;
            else
            {
                res = a;
                for (int i = 1; i < b; i++)
                {
                    res *= a;
                }
            }
            //long res = (long)Math.Pow(a, b);
            sw.Stop();
            Console.WriteLine(res);
            Console.WriteLine($"Час виконання функції:{sw.Elapsed}");

        }
    }
}
